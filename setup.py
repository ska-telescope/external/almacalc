from os import path
from glob import glob

# First import setuptools to enable the bdist_wheel command
from setuptools import find_packages
# Now use NumPy version of Extension (and setup) for its Fortran capabilities
from numpy.distutils.core import Extension, setup   # noqa: F811

# Read the contents of README.md file
this_directory = path.abspath(path.dirname(__file__))
with open(path.join(this_directory, 'README.md')) as readme_file:
    long_description = readme_file.read()

extension = Extension(
    name='almacalc._calc11',
    sources=['src/extension/calc11.pyf'] + glob('src/calc11/*.f'),
    # CALC has a non-standard mix of *.f files (F77) in free form (F90)
    extra_f77_compile_args=['-ffree-form', '-ffree-line-length-none',
                            # Suppress most warnings, typically due to commented-out code
                            '-Wno-unused', '-Wno-unused-dummy-argument'],
)

setup(
    name='almacalc',
    version='11.0.0',
    url='https://gitlab.com/ska-telescope/almacalc.git',
    maintainer='Ludwig Schwardt',
    maintainer_email='ludwig@ska.ac.za',
    description='Calculate radio interferometer delays using '
                'the venerable CALC 11 adapted to the ALMA telescope',
    long_description=long_description,
    long_description_content_type='text/markdown',
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Intended Audience :: Science/Research',
        'License :: OSI Approved :: BSD',
        'Operating System :: POSIX',
        'Operating System :: Unix',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: 3.9',
        'Topic :: Scientific/Engineering :: Astronomy',
    ],
    keywords=['astronomy', 'calc', 'alma'],
    license='3-Clause BSD',
    # Find all packages under src and remap the package directories
    packages=find_packages('src'),
    package_dir={'': 'src'},
    # With NumPy's setup you had better use package_data to get data into a wheel
    package_data={'': ['data/DE421_little_Endian']},
    ext_modules=[extension],
    zip_safe=False,
    python_requires='>=2.7, !=3.0.*, !=3.1.*, !=3.2.*, !=3.3.*, <4',
    install_requires=['numpy', "importlib_resources; python_version<'3.7'"],
    extras_require={'highlevel': ['astropy>=4.0', 'pyerfa']},
)
