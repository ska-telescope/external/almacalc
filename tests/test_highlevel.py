import os

import pytest
import numpy as np
import astropy
import astropy.units as u
from astropy.time import Time
from astropy.coordinates import EarthLocation, ICRS
from astropy.utils import iers
from packaging import version

from almacalc import highlevel


@pytest.mark.skipif(version.parse(astropy.__version__) < version.parse('4.0'),
                    reason='Requires Astropy 4.0 for EOP tables (and hence Python 3)')
def test_calc():
    # 1 antenna, 2 times
    ref = EarthLocation.from_geocentric(1.0, 1.299, 0.7, unit=u.km)
    ants = EarthLocation.from_geocentric(-3101.52, -11245.77, 8916.26, unit=u.m)
    temperature = 303.15 * u.K
    pressure = 1.0 * u.bar
    relative_humidity = 70 * u.percent
    obstime = Time([58701.45833333334, 58701.49930555555], format='mjd')
    source = ICRS(ra=2.0 * u.rad, dec=1.0 * u.rad)
    # Set all Earth orientation parameters to zero via a custom IERS_A file
    zero_eops_filename = os.path.join(os.path.split(__file__)[0], 'zero_eops.all')
    zero_eops = iers.IERS_A.open(zero_eops_filename)
    with iers.earth_orientation_table.set(zero_eops):
        delay = highlevel.calc(ants, source, obstime, ref,
                               temperature, pressure, relative_humidity)
    # These values are taken from test_almacalc.out
    geo_expected = np.array([[-1.6456996715386752e-5], [-2.2521328279609168e-5]])
    dry_expected = np.array([[3.1142312389072535e-9], [3.0580114084074736e-9]])
    wet_expected = np.array([[1.1857223257447686e-9], [1.1411305051149612e-9]])
    expected_delay = geo_expected + dry_expected + wet_expected
    expected_delay = expected_delay.reshape(obstime.shape + ants.shape) * u.s
    assert np.allclose(delay, expected_delay, rtol=0, atol=1e-6 * u.ps)

    # Change time shape to something more exciting to see if we can recover it
    with iers.earth_orientation_table.set(zero_eops):
        delay = highlevel.calc(ants, source, obstime.reshape((1, 1, -1)), ref,
                               temperature, pressure, relative_humidity)
    assert np.allclose(delay, expected_delay.reshape((1, 1, -1)), rtol=0, atol=1e-6 * u.ps)
